package ru.t1.amsmirnov.taskmanager.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.NoRepositoryBean;
import ru.t1.amsmirnov.taskmanager.dto.model.AbstractUserOwnedModelDTO;

import java.util.List;

@NoRepositoryBean
public interface AbstractUserOwnedDtoRepository<M extends AbstractUserOwnedModelDTO> extends AbstractDtoRepository<M> {

    long countByUserId(@NotNull final String userId);

    void deleteByUserId(@NotNull final String userId);

    void deleteByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    @NotNull
    List<M> findAllByUserId(@NotNull final String userId);

    @NotNull
    List<M> findAllByUserId(@NotNull final String userId, @NotNull final Sort sort);

    @Nullable
    M findByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    boolean existsByUserIdAndId(@NotNull final String userId, @NotNull final String id);

}
