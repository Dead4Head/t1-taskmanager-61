package ru.t1.amsmirnov.taskmanager.dto.response.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.dto.response.AbstractResultResponse;

public final class DataXmlLoadJaxBResponse extends AbstractResultResponse {

    public DataXmlLoadJaxBResponse() {
    }

    public DataXmlLoadJaxBResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}
