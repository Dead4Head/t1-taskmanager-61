package ru.t1.amsmirnov.taskmanager.dto.response.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.dto.response.AbstractResultResponse;

public final class DataYamlLoadFasterXMLResponse extends AbstractResultResponse {

    public DataYamlLoadFasterXMLResponse() {
    }

    public DataYamlLoadFasterXMLResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}
