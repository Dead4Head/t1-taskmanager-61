package ru.t1.amsmirnov.taskmanager.dto.model;

import org.jetbrains.annotations.NotNull;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@MappedSuperclass
public abstract class AbstractModelDTO implements Serializable {

    @Id
    @NotNull
    protected String id = UUID.randomUUID().toString();

    @NotNull
    @Column(name = "created", nullable = false)
    private Date created = new Date();

    @NotNull
    public String getId() {
        return id;
    }

    public void setId(@NotNull final String id) {
        this.id = id;
    }

    @NotNull
    public Date getCreated() {
        return created;
    }

    public void setCreated(@NotNull final Date created) {
        this.created = created;
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof AbstractModelDTO)) return false;
        return getId().equals(((AbstractModelDTO) obj).getId());
    }

}
